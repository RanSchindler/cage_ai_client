import cage_ai_creator.actions as actions

'''
A bot examples
'''


def random_brain(api):
    try:
        import src.bot.actions as actions
    except:
        import cage_ai_creator.actions as actions

    import random
    actions_options = [
        actions.NoAction(),
        actions.Move(direction='UP',    step=1),
        actions.Move(direction='UP',    step=2),
        actions.Move(direction='UP',    step=3),
        actions.Move(direction='DOWN',  step=1),
        actions.Move(direction='DOWN',  step=2),
        actions.Move(direction='DOWN',  step=3),
        actions.Move(direction='LEFT',  step=1),
        actions.Move(direction='LEFT',  step=2),
        actions.Move(direction='LEFT',  step=3),
        actions.Move(direction='RIGHT', step=1),
        actions.Move(direction='RIGHT', step=2),
        actions.Move(direction='RIGHT', step=3),
        actions.Attack(player=api.get_strongest_player()),
        actions.AttackMove(direction='UP',    player=api.get_strongest_player()),
        actions.AttackMove(direction='DOWN',  player=api.get_weakest_player()),
        actions.AttackMove(direction='LEFT',  player=api.get_strongest_player()),
        actions.AttackMove(direction='RIGHT', player=api.get_weakest_player()),
        actions.MoveAttack(direction='UP',    player=api.get_strongest_player()),
        actions.MoveAttack(direction='DOWN',  player=api.get_weakest_player()),
        actions.MoveAttack(direction='LEFT',  player=api.get_strongest_player()),
        actions.MoveAttack(direction='RIGHT', player=api.get_weakest_player()),
    ]

    # All possible queries you can run
    all_players = api.get_players()
    strongest = api.get_strongest_player()
    weakest = api.get_weakest_player()
    strong_location = api.get_player_location(strongest)
    weak_location = api.get_player_location(weakest)
    strong_life = api.get_player_life(strongest)
    weak_life = api.get_player_life(weakest)
    my_location = api.get_my_location()
    my_life = api.get_my_life()
    return actions_options[random.randint(0,len(actions_options)-1)]



'''
Now you will write your awesome bot
'''

def super_powerful_ai_warrior(api):
    '''     Don't touch this try catch block    '''
    try:
        import src.bot.actions as actions
    except:
        import cage_ai_creator.actions as actions

    '''     Write in this section all the imports you need     '''

    '''    Write your function here.     Good luck :)     '''

    #Code goes HERE....

    pass

'''
Fill this section with the right arena data
'''
ARENA = "5bf7a21af946dd1294dbecbe"
BOT_ID = "ef03f424-72c2-45f0-8d35-4a3f6dbfdc0d"
ARENA_ADDRESS = '127.0.0.1:5000'
WARRIOR_FUNCTION = random_brain

''' ''' ''' ''' ''' ''' ''' ''' ''' ''' ''' ''' ''' ''' '''
Don't touch from this point forward

Run this file to upload the BOT AI brain to the arena
''' ''' ''' ''' ''' ''' ''' ''' ''' ''' ''' ''' ''' ''' '''

import requests
import dill
import json
super_powerful_ai_warrior_ready = str(dill.dumps(WARRIOR_FUNCTION))

data = {
    "game_id": ARENA,
    "player_id": BOT_ID,
    "brain": super_powerful_ai_warrior_ready
}
r = requests.post('http://'+ARENA_ADDRESS+'/store_brain', data = json.dumps(data), headers={'Content-Type':'application/json'})
print("Status: ",r.status_code)
print("Response: ",r.json())

#Delete
data = {
	"game_id": ARENA,
    "player_id": "4e3ac9c0-9d92-41b0-a26a-288b6246bbc6",
    "brain": super_powerful_ai_warrior_ready
}
r = requests.post('http://'+ARENA_ADDRESS+'/store_brain', data = json.dumps(data), headers={'Content-Type':'application/json'})
